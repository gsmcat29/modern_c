// ternary operator sample
#include <stdio.h>
#include <stdlib.h>
#include <tgmath.h>
#include <math.h>
#include <complex.h>

#ifdef __STDC_NO_COMPLEX__
# error "we need complex arithmetic"
#endif

double complex sqrt_real(double x)
{
    return (x < 0) ? CMPLX(0, sqrt(-x)) : CMPLX(sqrt(x), 0);
}

int main()
{
    double complex root = 0;
    //double complex value = 0;
    double complex z1= 1.0 + 3.0 * I;

    //printf("Please enter a value: ");
    //scanf("%lf", &value);
    printf("Evaluating z1 = 1.0 + 3.0i\n");


    root = sqrt_real(z1);
    //printf("Result: %.2f\n", root);
    printf("Result: %lf + %lf\n", creal(root), cimag(root));

    return 0;
}

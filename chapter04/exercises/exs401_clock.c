// [Exs 1] Implement some computations using a 24-hour clock, 
// such as 3 hours after 10:00 and 8 hours after 20:00

#include <stdio.h>
#include <stdlib.h>

int main()
{
    // example using 12-hour format
    // for unsigned value, a == (a/b)*b + (a%b)
    size_t hour_12;
    size_t hour_24;
    // 6 hours after 8:00
    hour_12 = (8 + 6) % 12;
    printf("6 hours after 8:00 is %zu:00\n", hour_12);

    // 3 after 10:00
    hour_24 = (10 + 3) % 24;
    printf("10 hours after 3:00 is %zu:00\n", hour_24);

    // 8 hours after 20:00
    hour_24 = (20 + 8) % 24;
    printf("8 hours after 20:00 is %zu:00\n", hour_24);

    return 0;
}

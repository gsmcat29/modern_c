// Try to imagine what happens when i has value 0 and is decremented
// by means of the operator --

// Answer: size_t is unsigned, so no negative numbers are allowed

#include <stdio.h>
#include <stdlib.h>

int main()
{
    for (size_t i = 9; i <= 9; --i) {
        printf("Value of i %zu\n", i);
    }

    return EXIT_SUCCESS;
}

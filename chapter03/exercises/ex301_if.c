// Add the if (i) condition to the program, and compare the output to 
// the previous

/**
* After running both versions, the result is the same
**/

#include <stdio.h>
#include <stdlib.h>

int main()
{
    // declarations
    double A[5] = {[0] = 9.0, [1] = 2.9, [4] = 3.E+25, [3] = 0.00007};

    for (size_t i = 0; i < 5; ++i) {
        printf("element %zu is %g, \tits square is %g\n", i, A[i], A[i]*A[i]);
    }

    return EXIT_SUCCESS;
}

/* [Exs 6] Test the example switch statement in a program. See what happens
* if you leave out some of the break statements */

#include <stdio.h>
#include <stdlib.h>

int main()
{
    char arg = ' ';
    printf("Type the first letter of a corvid family: ");
    scanf("%c", &arg);

    switch (arg) {
        case 'm': puts("this is a magpie");
                  break;
        case 'r': puts("this is a raven");
                  break;
        case 'j': puts("this is a jay");
                  break;
        case 'c': puts("this is a chough");
                  break;
        default: puts("this is an unknow corvid");
    }

    puts("END OF PROGRAM");

    return EXIT_SUCCESS;
}

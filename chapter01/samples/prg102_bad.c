// This may look like nonses, but reeally is mode c

// the main thing that this program does
#include <stdio.h>

int main()
{
    // declarations
    int i;
    double A[5] = {
        9.0,
        2.9,
        3.E+25,
        0.00007
    };

    // doing some work
    for (i = 0; i < 5; ++i) {
        printf("element %d is %g, \tits square is %g\n", i, A[i], A[i]*A[i]);
    }

    return 0;
}

/**
* Try the compilation command in your terminal
*/

// This may look like nonsense, but really is mode: c
#include <stdlib.h>
#include <stdio.h>

// The main thing that this program does
int main(void)
{
    // declarations
    double A[5] = {
        [0] = 9.0,
        [1] = 2.9,
        [4] = 3.E+25,
        [3] = 0.00007, 
    };

    // doing some work
    for (size_t i = 0; i < 5; ++i) {
        printf("elment %zu is %g, \tits square in %g\n", i, A[i], A[i]*A[i]);
    }

    return EXIT_SUCCESS;
}

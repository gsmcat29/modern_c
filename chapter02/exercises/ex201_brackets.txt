Find these different uses of the two sorts of brackets:

// assuming listing 1.1
{}  - Curly brackets

* main scope
* Array grouping
* for loop with multiple statements


[]  - Square brackets

* Array declaration
* Array assignment
* Array transverse
